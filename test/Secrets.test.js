const Secrets = artifacts.require("Secrets");
const hex = require('./helpers/hex');
const { assert, expect } = require('chai');
const { expectRevert } = require('@openzeppelin/test-helpers')

require('chai')
  .use(require('chai-as-promised'))
  .should()


contract('Secrets', ([_, owner, ...others]) => {
  let secrets;

  describe('Retrieving shares', () => {

    beforeEach('setup contract for each test', async () => {
      secrets = await Secrets.new()
      await secrets.storeShares(hex.epk1, hex.c1, [hex.s1, hex.s2, hex.s3], [hex.lk1, hex.lk2, hex.lk3])
    })

    it('deploys successfully', async () => {
      const address = await secrets.address
      assert.notEqual(address, 0x0)
      assert.notEqual(address, '')
      assert.notEqual(address, null)
      assert.notEqual(address, undefined)
    })

    // FIXME: access built-in getter function for name
    it('has a name', async () => {
      const name = await secrets.name()
      assert.equal(name, 'Dark Crystal Social Backup')
    })

    it('should correctly save a ciphertext', async () => {
      const { text } = await secrets.getShare(hex.lk1)
      expect(text).to.equal(hex.c1)
    });

    it('should correctly retrieve a share', async () => {
      const { share } = await secrets.getShare(hex.lk1)
      expect(share).to.equal(hex.s1)
    })

    it('should correctly retrieve the ephemeral public key', async () => {
      const { epubkey } = await secrets.getShare(hex.lk1)
      expect(epubkey).to.equal(hex.epk1)
    })

    // FIXME: add a revert for this in the contract
    it('should fail if the lookupKey is not a hex', async () => {
      await secrets.getShare(hex.notHex).should.be.rejected
    });

    it('should fail if the lookupKey does not exist', async () => {
      await expectRevert(secrets.getShare(hex.fakeLK), "lookupKey does not exist")
    });
  })

  describe('Storage errors', async () => {

    beforeEach('setup contract for each test', async () => {
      secrets = await Secrets.new()
    })

    // FIXME: add a revert for this in the contract
    it('should fail if a share is not hex', async () => {
      await secrets
        .storeShares(hex.epk1, hex.c1,
          [hex.s1, hex.s2, hex.notHex],
          [hex.lk1, hex.lk2, hex.lk3]
        ).should.be.rejected;
    });

    // FIXME: add a revert for this in the contract
    it('should fail if a lookupKey is not hex', async () => {
      await secrets
        .storeShares(
          hex.epk1, hex.c1,
          [hex.s1, hex.s2, hex.s3],
          [hex.lk1, hex.lk2, hex.notHex])
        .should.be.rejected;
    });

    it('should fail if the shares array and lookup keys array are different lengths', async () => {
      await expectRevert(secrets
        .storeShares(
          hex.epk1, hex.c1,
          [hex.s1, hex.s2, hex.s3],
          [hex.lk1, hex.lk2]
        ), "number of shares does not match number of lookup keys");
    });

    it('should fail if a ciphertext already exists', async () => {
      await secrets
        .storeShares(
          hex.epk1, hex.c1,
          [hex.s1, hex.s2, hex.s3],
          [hex.lk1, hex.lk2, hex.lk3]
        )
      await expectRevert(secrets
        .storeShares(
          hex.epk2, hex.c1,
          [hex.s4, hex.s5, hex.s6],
          [hex.lk4, hex.lk5, hex.lk6]
        ), "ciphertext already exists");
    })

    it('should fail if a lookup key already exists', async () => {
      await secrets
        .storeShares(
          hex.epk1, hex.c1,
          [hex.s1, hex.s2, hex.s3],
          [hex.lk1, hex.lk2, hex.lk3]
        )
      await expectRevert(secrets
        .storeShares(
          hex.epk2, hex.c2,
          [hex.s4, hex.s5, hex.s6],
          [hex.lk4, hex.lk5, hex.lk3]
        ), "lookupKey already in use");
    })
  })
})
