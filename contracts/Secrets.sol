// SPDX-License-Identifier: GPL-3.0-or-later

pragma solidity ^0.8.0;

contract Secrets {
    string public constant name = "Dark Crystal Social Backup";
    // bytes32 is a ciphertextId; the Text contains the associated ciphertext and its ephemeral public key
    mapping(bytes32 => Text) private ciphertexts;
    // bytes32 is an individual recovery partner's lookupkey; the Backup contains the associated ciphertextId and their share
    mapping(bytes32 => Backup) private backups;

    struct Text {
        // save one copy of the ephemeral public key, needed to decrypt each share
        bytes32 ephPubKey;
        // save one copy of the ciphertext
        bytes ciphertext;
        // add an 'exists' value to prevent overwriting
        bool exists;
    }

    struct Backup {
        // pointer to the associated Text struct
        bytes32 ciphertextId;
        // recovery partner's individual share
        bytes share;
        // add an 'exists' value to prevent overwriting
        bool exists;
    }

    // FIXME: Do we want to set a flag on a secret or on individual shares to tell when/whether it has been retrieved already => does this indicate something useful?

    /*
@dev
@dev run check that transaction payload contains matching number of keys and shares

@param epk  The ephemeral public key
@return
*/

    function storeShares(
        bytes32 epk,
        bytes memory ct,
        bytes[] memory shares,
        bytes32[] memory lukeys
    ) public {
        // CHECK ONE: check that shares array and lukeys array are the same length
        require(
            shares.length == lukeys.length,
            "number of shares does not match number of lookup keys"
        );
        // FIXME: does solidity have async functions - can we await ctId?
        bytes32 ctId = setCiphertext(epk, ct);
        setSharesForCiphertext(shares, lukeys, ctId, ct);
    }

    function setCiphertext(bytes32 ephKey, bytes memory cipherText)
        private
        returns (bytes32 ciphertextId)
    {
        // hash the cipherText to create bytes32 ciphertextId
        ciphertextId = keccak256(bytes(cipherText));
        // CHECK TWO: check that the ciphertext does not already exist => do not overwrite an existing ciphertext
        require(
            ciphertexts[ciphertextId].exists != true,
            "ciphertext already exists"
        );
        Text memory newText;
        // {exists: true} allows for checking that we are not overwriting an existing secret. this should be close to impossible, because of the way the secret is originally encrypted, but this check adds an extra layer of assurance.
        newText = Text({
            ciphertext: cipherText,
            ephPubKey: ephKey,
            exists: true
        });
        ciphertexts[ciphertextId] = newText;
        return (ciphertextId);
    }

    function setSharesForCiphertext(
        bytes[] memory allShares,
        bytes32[] memory lookupKeys,
        bytes32 ciphertextId,
        bytes memory cipherText
    ) private {
        // CHECK THREE: check that the stored ciphertext is the correct one for these shares
        bytes memory retrievedText = ciphertexts[ciphertextId].ciphertext;
        assert(keccak256(bytes(retrievedText)) == keccak256(bytes(cipherText)));
        // create a new Backup
        Backup memory newBackup;
        for (uint256 i = 0; i < lookupKeys.length; i++) {
            // CHECK FOUR: check that a lookupKey does not already exist => do not overwrite an existing lookup key
            // NOTE: a lookup-key will already exist EITHER IF a secret-owner re-uses a 'phrase' to identify a new secret && the recovery partner uses the same ethereum address for their share of that secret. OR IF a secret-owner uses a phrase that another secret-owner has used before && the two owners share a recovery partner in common who uses the same ethereum address for their share of both secrets.
            // FIXME: should this revert the transaction? or publish an event to let the secret owner know?
            require(
                backups[lookupKeys[i]].exists != true,
                "lookupKey already in use"
            );
            // assign ciphertextId and share to new backup
            newBackup = Backup({
                ciphertextId: ciphertextId,
                share: allShares[i],
                exists: true
            });
            backups[lookupKeys[i]] = newBackup;
        }
    }

    // the lookupKey is derived from an identifier unique to a given secret, that is set by the secret-owner when they initially shard the secret. the identifier is combined with each recovery-partner's public key to create a unique lookupKey associated with each share. the unique identifier is passed, by the secret-owner to the recovery partners, when they need to retrieve their share.
    function getShare(bytes32 lookupKey)
        public
        view
        returns (
            bytes memory share,
            bytes memory text,
            bytes32 epubkey
        )
    {
        // CHECK FIVE: check that the backup exists before attempting to return it
        require(backups[lookupKey].exists == true, "lookupKey does not exist");
        bytes32 cId = backups[lookupKey].ciphertextId;
        // CHECK SIX: check that the ciphertext exists before attempting to return it
        require(ciphertexts[cId].exists == true, "ciphertextId does not exist");
        return (
            backups[lookupKey].share,
            ciphertexts[cId].ciphertext,
            ciphertexts[cId].ephPubKey
        );
    }
}
