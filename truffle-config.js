const HDWalletProvider = require("@truffle/hdwallet-provider");
const fs = require("fs");
const secrets = JSON.parse(fs.readFileSync(".secrets.json").toString().trim());

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" // Match any network id
    },
    goerli: {
      networkCheckTimeout: 10000,
      provider: () => {
        return new HDWalletProvider(
          secrets.mnemonic,
          `wss://goerli.infura.io/ws/v3/${secrets.projectId}`
        );
      },
      network_id: '5',
      gas: 4465030
    }
  },
  compilers: {
    solc: {
      version: "0.8.11",
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
}
